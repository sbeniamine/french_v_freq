# French verbal frequencies in the Open Subtitle corpus

[![DOI](https://zenodo.org/badge/doi/10.5281/zenodo.6412876.svg)](http://dx.doi.org/10.5281/zenodo.6412876)

This repository provides frequencies for French verbal paradigm cells in
the [French Open Subtitles Corpus](https://aclanthology.org/L16-1147/).

To obtain these frequencies we:

- Trained a tagger based on [Flaubert-base-cased](https://huggingface.co/flaubert/flaubert_base_cased) (see [FlauBERT](https://aclanthology.org/2020.lrec-1.302/))
 on the [French_GSD part of the Universal Dependency treebank](https://universaldependencies.org/treebanks/fr_gsd/index.html). In order to improve performances
 on rarer cells, the corpus was augmented with synthetic sentences which we generated using manually written templates and French  verbal orthographic data from Demonext.
- Cleaned and tagged the Open Subtitle Corpus
- Counted the occurences of each cells for words tagged as VERB or AUX
- Kept only the predicted feature combinations which were valid French verbal paradigm cells.
- Converted to the paradigm cell scheme used
  in [Vlexique](http://www.llf.cnrs.fr/fr/flexique-fr.php)


The frequencies are extracted from a 401360174 token corpus.
We found a total of 74148211, from which 415176 
occurences were excluded because of inexistant paradigm cells (prediction errors).
73733035 remaining occurences are documented.


- published under the license: [GPL-3.0](https://opensource.org/licenses/GPL-3.0)
- version: 1.0.0
- Contributors: 
    - Sacha Beniamine (author)
    - Maximin Coavoux (author)
    - Olivier Bonami (author)

Please cite as: Sacha Beniamine, Maximin Coavoux & Olivier Bonami (2022), French verbal frequencies in the Open Subtitle corpus. DOI: 10.5281/zenodo.6412876

# Files
## frequencies (frequencies_subtitles.csv)

French paradigm cell frequencies

- `cell` (string): paradigm cell, in the vlexique notation.
- `freq` (integer): total raw frequency for this cell.